package com.fedor.pavel.econtact.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import com.fedor.pavel.econtact.fragments.ClaimsListFragment;
import com.fedor.pavel.econtact.models.RequestModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClaimsNavVPAdapter extends FragmentStatePagerAdapter {

    private List<RequestModel> mItems;

    public ClaimsNavVPAdapter(FragmentManager fm, List<RequestModel> items) {
        super(fm);
        mItems = items;
    }

    @Override
    public Fragment getItem(int position) {

        return ClaimsListFragment.newInstance(mItems.get(position),position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mItems.get(position).getTitle();
    }


}
