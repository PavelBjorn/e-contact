package com.fedor.pavel.econtact.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

public abstract class ProgressActivity extends AppCompatActivity {

    protected ProgressDialog mProgressDialog;

    protected final void setProgressEnable(){
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
    }

    protected final void showProgress(){
        if (mProgressDialog!=null&&!mProgressDialog.isShowing()){
            mProgressDialog.show();
        }
    }

    protected final void dismissProgress(){
        if (mProgressDialog!=null&&mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

}
