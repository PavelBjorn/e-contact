package com.fedor.pavel.econtact.activities;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.fedor.pavel.econtact.R;
import com.fedor.pavel.econtact.adapters.RVPhotosAdapter;
import com.fedor.pavel.econtact.date.DateParseManager;
import com.fedor.pavel.econtact.fragments.ClaimsListFragment;
import com.fedor.pavel.econtact.models.ClaimModel;
import com.fedor.pavel.econtact.models.PerformersModel;
import com.fedor.pavel.econtact.views.RVDivider;
import com.google.gson.Gson;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ClaimFullInfoActivity extends AppCompatActivity {

    @Bind(R.id.activity_full_info_rvPhotos)
    RecyclerView mRvPhotos;

    @Bind(R.id.activity_full_info_tvServices)
    TextView mTvServices;

    @Bind(R.id.activity_full_info_tvCreatedItem)
    TextView mTvCreated;

    @Bind(R.id.activity_full_info_tvExecuteItem)
    TextView mTvExecute;

    @Bind(R.id.activity_full_info_tvChargeItem)
    TextView mTvCharge;

    @Bind(R.id.activity_full_info_tvRegItem)
    TextView mTvRegistered;

    @Bind(R.id.activity_full_info_tvIndicator)
    TextView mTvIndicator;

    @Bind(R.id.activity_full_info_tvDescription)
    TextView mTvDescription;

    private ClaimModel mClaimModel;
    private RVPhotosAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_info);

        ButterKnife.bind(this);
        mClaimModel = new Gson().fromJson(getIntent().getStringExtra(ClaimsListFragment.BUNDLE_CLAIM_KEY), ClaimModel.class);
        fillViews();
        prepareProgressIndicatorTextView();
        prepareRecyclerView();
        prepareToolbar();

    }

    private void fillViews() {
        mTvServices.setText(mClaimModel.getCategory().getTitle());
        mTvCreated.setText(DateParseManager.parseDate(mClaimModel.getCreatedDate(), DateParseManager.DAY_MONTH_YEAR_DATE_PATTERN));
        mTvRegistered.setText(DateParseManager.parseDate(mClaimModel.getStartedDate(), DateParseManager.DAY_MONTH_YEAR_DATE_PATTERN));
        PerformersModel performersModel = mClaimModel.getInCharge();
        if (performersModel != null) {
            mTvCharge.setText(performersModel.getOrganization());
        }
        mTvExecute.setText(DateParseManager.parseDate(mClaimModel.getExecuteUntil(), DateParseManager.DAY_MONTH_YEAR_DATE_PATTERN));
        mTvDescription.setText(mClaimModel.getDescription());
    }

    private void prepareProgressIndicatorTextView() {
        mTvIndicator.setBackground(getResources()
                .getDrawable(getIntent().getIntExtra(ClaimsListFragment.STATE_INDICATOR_RES_ID_BUNDLE_KEY,R.drawable.indicator_in_progress)));
        mTvIndicator.setText(mClaimModel.getWorkStatus().getTitle());
    }

    private void prepareRecyclerView() {
        if (mClaimModel.getNumOfPhoto() > 0) {
            mAdapter = new RVPhotosAdapter(this, mClaimModel.getAllURLs());
            mRvPhotos.setAdapter(mAdapter);
            mRvPhotos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mRvPhotos.addItemDecoration(new RVDivider(this, R.dimen.activity_horizontal_margin, LinearLayoutManager.HORIZONTAL));
        } else {
            mRvPhotos.setVisibility(View.GONE);
        }
    }

    private void prepareToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(mClaimModel.getClaimRegisteredId());
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) { //  I made such method for grouping TextViews by click event type
        switch (v.getId()) {
            case R.id.activity_full_info_tvServices:
            case R.id.activity_full_info_tvCreatedItem:
            case R.id.activity_full_info_tvExecuteItem:
            case R.id.activity_full_info_tvChargeItem:
            case R.id.activity_full_info_tvRegItem:
            case R.id.activity_full_info_tvIndicator:
            case R.id.activity_full_info_tvChargeTitle:
            case R.id.activity_full_info_tvCreatedTitle:
            case R.id.activity_full_info_tvExecuteTitle:
            case R.id.activity_full_info_tvRegTitle:
            case R.id.activity_full_info_tvDescription:

                Snackbar.make(v, v.getClass().getSimpleName(), Snackbar.LENGTH_SHORT).show();
                break;
        }
    }

}
