package com.fedor.pavel.econtact.data;




public interface OnDataLoadedListener <T> {
    void dataLoadingCompleted(T result);
    void loadingFailed(Throwable e);
    void loadingStarted();
}
