package com.fedor.pavel.econtact.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fedor.pavel.econtact.R;
import com.fedor.pavel.econtact.adapters.ClaimsNavVPAdapter;
import com.fedor.pavel.econtact.models.RequestModel;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AllClaimsNavFragment extends Fragment {

    @Bind(R.id.fragment_all_claims_vp_claimsLIst)
    ViewPager mVpClaimsLIst;

    @Bind(R.id.fragment_all_claim_tbl)
    SmartTabLayout mTbaNavigation;

    public static final String LOG_TAG = "AllClaimsNavFragment";

    private ClaimsNavVPAdapter mClaimsNavVPAdapter;

    public static final int VIEW_PAGER_OFFSCREEN_PAGE_LIMIT = 2;

    public static AllClaimsNavFragment newInstance() {
        return new AllClaimsNavFragment();
    }

    private void prepareViews(View view) {
        ButterKnife.bind(this, view);
        prepareViewPager();
        prepareTabLayout();
    }

    private void prepareViewPager() {
        mClaimsNavVPAdapter = new ClaimsNavVPAdapter(getActivity().getSupportFragmentManager(), createRequestModels());
        mVpClaimsLIst.setAdapter(mClaimsNavVPAdapter);
        mVpClaimsLIst.setOffscreenPageLimit(VIEW_PAGER_OFFSCREEN_PAGE_LIMIT);
    }

    private void prepareTabLayout() {
        mTbaNavigation.setViewPager(mVpClaimsLIst);
    }

    private List<RequestModel> createRequestModels() {
        List<RequestModel> models = new ArrayList<>();

        models.add(new RequestModel(getContext().getResources().getText(R.string.claimStatusInProgressTitle).toString()
                , RequestModel.CLAIMS_STATE_IN_PROGRESS_VALUES, R.drawable.indicator_in_progress));
        models.add(new RequestModel(getContext().getResources().getText(R.string.claimStatusCompletedTitle).toString()
                , RequestModel.CLAIMS_STATE_DONE_VALUES, R.drawable.indicator_completed));
        models.add(new RequestModel(getContext().getResources().getText(R.string.claimStatusUncompletedTitle).toString()
                , RequestModel.CLAIMS_STATE_PENDING_VALUES, R.drawable.indicator_uncompleted));

        return models;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_all_claims, container, false);
        prepareViews(view);

        return view;
    }

}
