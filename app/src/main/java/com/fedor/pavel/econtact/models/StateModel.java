package com.fedor.pavel.econtact.models;


import android.support.annotation.StringRes;

import com.fedor.pavel.econtact.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;


public class StateModel implements Serializable {


    @Expose
    @SerializedName(ApiFields.NAME_FIELD)
    private String mTitle;

    @Expose
    @SerializedName(ApiFields.ID_FIELD)
    private int mIdServer;

    public StateModel(String mTitle, int mIndicatorResId) {
        this.mTitle = mTitle;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(@StringRes String mTitle) {
        this.mTitle = mTitle;
    }

    public int getIndicatorResId() {

        int index = Arrays.asList(RequestModel.CLAIMS_STATE_IN_PROGRESS_VALUES).indexOf(mIdServer);
        if (index!=-1) {
            return R.drawable.indicator_in_progress;
        }
        if (Arrays.asList(Arrays.toString(RequestModel.CLAIMS_STATE_DONE_VALUES)).indexOf(mIdServer)!=-1) {
            return R.drawable.indicator_completed;
        }
        if (Arrays.asList(RequestModel.CLAIMS_STATE_PENDING_VALUES).indexOf(mIdServer)!=-1) {
            return R.drawable.indicator_uncompleted;
        }

        return 0;
    }

    public static class ApiFields {
        public static final String NAME_FIELD = "name";
        public static final String ID_FIELD = "id";
    }

}
