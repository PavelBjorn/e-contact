package com.fedor.pavel.econtact.data;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.facebook.AccessToken;
import com.fedor.pavel.econtact.models.ClaimModel;
import com.fedor.pavel.econtact.models.UserModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import rx.Observable;
import rx.functions.Func1;

public class SqlManager {
    private SqlHelper mSqlHelper;
    private static boolean mIsInit = false;
    private SQLiteDatabase mSqLiteDatabase;
    private static SqlManager mInstance;

    private SqlManager(Context context) {
        if (mSqlHelper == null) {
            mSqlHelper = new SqlHelper(context);
            mIsInit = true;
        }
    }

    public static void initialize(Context context) {
        mInstance = new SqlManager(context);
    }

    public static SqlManager getInstance() {
        if (mIsInit) {
            return mInstance;
        } else {
            throw new ExceptionInInitializerError("SqlManager wasn't initialized. Call initialize(context) first");
        }
    }

    public void open() {
        mSqLiteDatabase = mSqlHelper.getWritableDatabase();
    }

    public void close() {
        if (mSqLiteDatabase.isOpen()) {
            mSqLiteDatabase.close();
        }
    }

    public long insert(String tableName, ContentValues values) {
        return mSqLiteDatabase.insert(tableName, null, values);
    }

    public boolean checkStatus(ClaimModel claimModel) {
        Cursor cursor = mSqLiteDatabase.query(SqlHelper.TABLE_NAME_CLAIMS,
                new String[]{SqlHelper.CLAIMS_ID_COLUMN}
                , SqlHelper.CLAIMS_SERVER_ID_COLUMN + "=\'" + claimModel.getServerId() + "\'"
                , null, null, null, null);
        return cursor.getCount() > 0;
    }

    public Func1<List<ClaimModel>, List<ClaimModel>> insertClaims() {

        return new Func1<List<ClaimModel>, List<ClaimModel>>() {
            @Override
            public List<ClaimModel> call(List<ClaimModel> claimModels) {
                mSqLiteDatabase.beginTransaction();
                for (ClaimModel claim : claimModels) {
                    if (!checkStatus(claim)) {
                        insert(SqlHelper.TABLE_NAME_CLAIMS, claim.getContentValues());
                    }
                }
                mSqLiteDatabase.setTransactionSuccessful();
                mSqLiteDatabase.endTransaction();
                return claimModels;
            }
        };
    }

    public Func1<UserModel, UserModel> insertUser() {
        return new Func1<UserModel, UserModel>() {
            @Override
            public UserModel call(UserModel userModel) {
                ContentValues contentValues = userModel.getContentValues();
                contentValues.put(SqlHelper.USER_FACEBOOK_ACCESS_TOKEN, new Gson().toJson(AccessToken.getCurrentAccessToken()));
                insert(SqlHelper.TABLE_NAME_USER, contentValues);
                return userModel;
            }
        };
    }

    public Observable<int[]> loadClaims(int[] queryParams) {
        return Observable.just(queryParams);
    }

    public Func1<String, Cursor> makeSelectQuery() {
        return new Func1<String, Cursor>() {
            @Override
            public Cursor call(String tableName) {
                return mSqLiteDatabase.query(tableName, null, null, null, null, null, null);
            }
        };
    }

    public Func1<int[], Cursor> makeClaimsSelectQuery() {
        return new Func1<int[], Cursor>() {
            @Override
            public Cursor call(int[] queryParams) {
                return mSqLiteDatabase.rawQuery(parseQueryParams("SELECT * FROM " + SqlHelper.TABLE_NAME_CLAIMS + " WHERE ", queryParams), null);
            }
        };
    }

    public Func1<Cursor, List<ClaimModel>> parseToClaimsModels() {
        return new Func1<Cursor, List<ClaimModel>>() {
            @Override
            public List<ClaimModel> call(Cursor cursor) {
                List<ClaimModel> claims = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    claims.add(new ClaimModel(cursor));
                    cursor.moveToNext();
                }
                cursor.close();
                return claims;
            }
        };
    }

    public Func1<Cursor, UserModel> parseToUserModel() {
        return new Func1<Cursor, UserModel>() {
            @Override
            public UserModel call(Cursor cursor) {
                UserModel userModel = null;
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    userModel = new UserModel(cursor);
                    cursor.moveToNext();
                }
                cursor.close();
                return userModel;
            }
        };
    }

    public boolean isTableHaveData(String tableName, int[] queryParams) {
        String query = " SELECT count(*) FROM " + tableName;
        Cursor cursor = mSqLiteDatabase.rawQuery(queryParams != null ? parseQueryParams(query + " WHERE ", queryParams) : query, null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        cursor.close();
        return count <= 0;
    }

    public void clearTableClaims(int[] queryParams) {
        mSqLiteDatabase.delete(SqlHelper.TABLE_NAME_CLAIMS, parseQueryParams("", queryParams), null);
    }

    private String parseQueryParams(String query, int[] queryParams) {
        if (queryParams != null) {
            for (int param : queryParams) {
                query += SqlHelper.CLAIMS_STATE_COLUMN + " LIKE " + " '%" + param + "%'";
                if (param != queryParams[queryParams.length - 1]) {
                    query += " OR ";
                }
            }
            return query;
        }
        return null;
    }

}
