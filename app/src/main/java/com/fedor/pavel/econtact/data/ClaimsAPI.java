package com.fedor.pavel.econtact.data;


import com.fedor.pavel.econtact.models.ClaimModel;



import java.util.List;
import java.util.Map;


import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface ClaimsAPI {

    @GET(QueryParams.CLAIMS_PARAM)
    Observable<List<ClaimModel>> getClaims(@QueryMap Map<String, String> params);


    interface QueryParams {
        String BASE_URL = "http://dev-contact.yalantis.com/rest/v1/";
        String PICTURE_URL = "http://dev-contact.yalantis.com/files/ticket/";
        String CLAIMS_PARAM = "tickets";
        String CLAIMS_STATE__PARAM = "state";
        String CLAIMS_AMOUNT_PARAM = "amount";
        String CLAIMS_OFFSET_PARAM = "offset";
    }
}
