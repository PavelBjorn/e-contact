package com.fedor.pavel.econtact.adapters;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.fedor.pavel.econtact.R;
import com.fedor.pavel.econtact.data.OnLoadNextListener;
import com.fedor.pavel.econtact.fragments.ClaimsListFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public abstract class DataRvAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected List<T> mItems;
    private int mFooterOffset = 0;
    protected boolean mIsFooterEnable = false;
    protected LayoutInflater mInflater;
    protected OnLoadNextListener mListener;
    private int mVisibleThreshold = 2;
    private boolean mIsLoadingData = false;


    public DataRvAdapter(Context context, List<T> items) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    public DataRvAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        mItems = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        if (mIsFooterEnable && position >= mItems.size()) {
            return ViewTyp.VIEW_TYPE_FOOTER;
        } else {
            return ViewTyp.VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size() + mFooterOffset;
    }


    public void addAll(List<T> items) {
        int startPosition = mItems.size();
        mItems.addAll(items);
        notifyItemRangeInserted(startPosition, items.size());
    }

    private void setFooterEnable(boolean isFooterEnable) {
        mIsFooterEnable = isFooterEnable;
        if (isFooterEnable) {
            mFooterOffset = 1;
            notifyItemInserted(getItemCount() - 1);
        } else {
            mFooterOffset = 0;
            notifyItemRemoved(getItemCount() - 1);
        }
    }

    public void loadWithPagination(RecyclerView rvParent, OnLoadNextListener listener) {
        mListener = listener;

        rvParent.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    pagination((LinearLayoutManager) recyclerView.getLayoutManager());
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void pagination(LinearLayoutManager linearLayoutManager) {
        int lastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();
        if (!mIsLoadingData
                && (lastVisibleItem + mVisibleThreshold >= mItems.size())
                && mListener.canLoadMore()) {
            setLoadingData(true);
            mListener.loadNextPart(mItems.size());
        }
    }

    public void setLoadingData(boolean isLoadingData) {
        mIsLoadingData = isLoadingData;
        setFooterEnable(isLoadingData);
    }

    public boolean isIsLoadingData() {
        return mIsLoadingData;
    }


    class ProgressFooterViewHolder extends RecyclerView.ViewHolder {
        public ProgressFooterViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ViewTyp {
        public static final int VIEW_TYPE_HEADER = 1;
        public static final int VIEW_TYPE_ITEM = 2;
        public static final int VIEW_TYPE_FOOTER = 3;
    }


}
