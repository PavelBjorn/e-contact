package com.fedor.pavel.econtact.views;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fedor.pavel.econtact.R;
import com.fedor.pavel.econtact.adapters.DataRvAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;



public class DrawerMenu extends RecyclerView {

    private MenuAdapter mAdapter;

    public DrawerMenu(Context context) {
       this(context,null,0);
    }

    public DrawerMenu(Context context, AttributeSet attrs) {
       this(context,attrs,0);
    }

    public DrawerMenu(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initDefaultAdapter();
    }

    private void initDefaultAdapter() {
        mAdapter = new MenuAdapter(getContext());
        mAdapter.setHeaderView(R.layout.nav_header_drawer_menu);
        mAdapter.setItemView(R.layout.nav_item_drawer_menu);
        mAdapter.setFooterView(R.layout.nav_footer_drawer_menu);
        createDefaultMenuItems();
        createDefaultFooterItems();
        setAdapter(mAdapter);
        addItemDecoration(new RvMenuItemDivider(getContext()));
        setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void createDefaultMenuItems() {
        String[] items = getContext().getResources().getStringArray(R.array.menu_item_titles);
        int[] iconResId = {R.drawable.ic_issues, R.drawable.ic_map};

        for (int i = 0; i < items.length; i++) {
            mAdapter.addItem(new MenuItem(items[i], iconResId[i]));
        }
    }

    private void createDefaultFooterItems() {
        String[] items = getContext().getResources().getStringArray(R.array.menu_footer_titles);
        for (String item : items) {
            mAdapter.addFooter(new MenuItem(item));
        }
    }

    public void setSelectedItem(int position) {
        mAdapter.setSelection(position);
    }

    public void setOnItemSelectedListener(OnDrawerMenuItemSelectedListener listener) {
        if (mAdapter != null) {
            mAdapter.setOnItemSelectedListener(listener);
        }
    }

    public class MenuItem {

        private String mTitle;
        private int mIconResId;

        public MenuItem(String mTitle) {
            this.mTitle = mTitle;
        }


        public MenuItem(String mTitle, int mIconResId) {
            this.mTitle = mTitle;
            this.mIconResId = mIconResId;
        }

        public String getTitle() {
            return mTitle;
        }

        public void setmTitle(String mTitle) {
            this.mTitle = mTitle;
        }

        public int getIconResId() {
            return mIconResId;
        }

        public void setmIconResId(int mIconResId) {
            this.mIconResId = mIconResId;
        }
    }

    public class MenuAdapter extends Adapter<ViewHolder> {

        protected int mHeaderLayoutRes;
        protected int mItemLayoutRes;
        protected int mFooterLayoutRes;
        protected List<MenuItem> mItems = new ArrayList<>();
        protected List<MenuItem> mFooters = new ArrayList<>();
        private LayoutInflater inflater;
        public static final byte HEADER_OFFSET = 1;
        private int mSelectedItem = -1;
        private OnDrawerMenuItemSelectedListener mOnDrawerMenuItemSelectedListener;

        public MenuAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        /**
         * Method for choose which item in {@link DrawerMenu} will be selected
         *
         * @param position - position of main menu item which you want to be selected
         * @throws IllegalArgumentException if {@code position < 0 || position >= mItems.size()}
         */
        public void setSelection(int position) throws IllegalArgumentException {
                mSelectedItem = position;
                notifyItemChanged(position);
        }

        public void setOnItemSelectedListener(OnDrawerMenuItemSelectedListener listener) {
            mOnDrawerMenuItemSelectedListener = listener;
        }

        public void addItem(MenuItem item) {
            mItems.add(item);
        }

        public void setItemView(@LayoutRes int viewId) {
            mItemLayoutRes = viewId;
        }

        public void setHeaderView(@LayoutRes int viewId) {
            mHeaderLayoutRes = viewId;
        }

        public void setFooterView(@LayoutRes int viewId) {
            mFooterLayoutRes = viewId;
        }

        public void addFooter(MenuItem footer) {
            mFooters.add(footer);
        }

        public int getRealItemPosition(int position) {
            return position - HEADER_OFFSET;
        }

        public int getRealFooterPosition(int position) {
            return position - HEADER_OFFSET - mItems.size();
        }

        public MenuItem getItem(int position) {
            if (getItemViewType(position) == DataRvAdapter.ViewTyp.VIEW_TYPE_ITEM) {
                return mItems.get(getRealItemPosition(position));
            } else {
                return mFooters.get(getRealFooterPosition(position));
            }
        }

        public int getOnlyFootersCount() {
            return mFooters.size();
        }

        @Override
        public int getItemViewType(int position) {
            if (position < HEADER_OFFSET) {
                return DataRvAdapter.ViewTyp.VIEW_TYPE_HEADER;
            } else if (!mFooters.isEmpty() && position >= mItems.size() + HEADER_OFFSET) {
                return DataRvAdapter.ViewTyp.VIEW_TYPE_FOOTER;
            } else {
                return DataRvAdapter.ViewTyp.VIEW_TYPE_ITEM;
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            if (viewType == DataRvAdapter.ViewTyp.VIEW_TYPE_HEADER) {
                return new HeaderViewHolder(inflater.inflate(mHeaderLayoutRes, parent, false));
            } else if (viewType == DataRvAdapter.ViewTyp.VIEW_TYPE_ITEM) {
                return new ItemViewHolder(inflater.inflate(mItemLayoutRes, parent, false));
            } else if (viewType == DataRvAdapter.ViewTyp.VIEW_TYPE_FOOTER) {
                return new FooterViewHolder(inflater.inflate(mFooterLayoutRes, parent, false));
            }

            return null;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (holder instanceof ItemViewHolder) {
                bindItemViewHolder((ItemViewHolder) holder, position);
            }
            if (holder instanceof FooterViewHolder) {
                bindFooterViewHolder((FooterViewHolder) holder, position);
            }
        }

        protected void bindItemViewHolder(ItemViewHolder holder, int position) {
            holder.mImvPhoto.setImageResource(mItems.get(getRealItemPosition(position)).getIconResId());
            holder.mTvTitle.setText(mItems.get(getRealItemPosition(position)).getTitle());
            holder.mContentContainer.setSelected(position == mSelectedItem);
        }

        protected void bindFooterViewHolder(FooterViewHolder holder, int position) {
            holder.mTvFooterTitle.setText(mFooters.get(getRealFooterPosition(position)).getTitle());
        }

        @Override
        public int getItemCount() {
            return 1 + mItems.size() + mFooters.size();
        }

        class HeaderViewHolder extends ViewHolder {
            public HeaderViewHolder(View itemView) {
                super(itemView);
            }
        }

        class ItemViewHolder extends ViewHolder {

            @Bind(R.id.item_drawer_menu_imv_icon)
            ImageView mImvPhoto;

            @Bind(R.id.item_drawer_menu_tv_title)
            TextView mTvTitle;

            @Bind(R.id.nav_item_drawer_menu_content_container)
            ViewGroup mContentContainer;

            public ItemViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @OnClick(R.id.nav_item_drawer_menu_content_container)
            public void onClick() {

                int previousSelectedItem = mSelectedItem;

                if (getAdapterPosition() != mSelectedItem) {
                    mSelectedItem = getAdapterPosition();
                    mContentContainer.setSelected(true);
                    notifyItemChanged(previousSelectedItem);

                    if (mOnDrawerMenuItemSelectedListener != null) {
                        mOnDrawerMenuItemSelectedListener.onMenuItemSelected(DrawerMenu.this, getAdapterPosition());
                    }
                }
            }
        }

        class FooterViewHolder extends ViewHolder {

            @Bind(R.id.drawer_menu_footer_tvTitle)
            TextView mTvFooterTitle;

            @Bind(R.id.drawer_menu_footer_content_container)
            FrameLayout mFooterContentContainer;

            public FooterViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @OnClick(R.id.drawer_menu_footer_content_container)
            public void onClick() {

                if (mOnDrawerMenuItemSelectedListener != null) {
                    mOnDrawerMenuItemSelectedListener.onMenuItemSelected(DrawerMenu.this
                            , getAdapterPosition());
                }
            }
        }

    }

    public interface OnDrawerMenuItemSelectedListener {
        void onMenuItemSelected(DrawerMenu parent, int position);
    }

}
