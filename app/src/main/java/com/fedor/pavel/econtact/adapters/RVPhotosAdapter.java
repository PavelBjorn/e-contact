package com.fedor.pavel.econtact.adapters;


import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fedor.pavel.econtact.R;
import com.fedor.pavel.econtact.models.ClaimModel;
import com.nostra13.universalimageloader.core.ImageLoader;


import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RVPhotosAdapter extends DataRvAdapter<ClaimModel.PhotosModel,RVPhotosAdapter.PhotosViewHolder> {


    public RVPhotosAdapter(Context context, List<ClaimModel.PhotosModel> items) {
        super(context, items);
    }

    @Override
    public PhotosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotosViewHolder(mInflater.inflate(R.layout.item_rv_photos, parent, false));
    }

    @Override
    public void onBindViewHolder(PhotosViewHolder holder, int position) {
        ImageLoader.getInstance().displayImage(mItems.get(position).getUrl(), holder.imvPhoto);
    }

    class PhotosViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.item_rv_photos_imvPhoto)
        ImageView imvPhoto;

        public PhotosViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imvPhoto.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.item_rv_photos_imvPhoto:
                    Snackbar.make(itemView, "ImageView", Snackbar.LENGTH_SHORT).show();
                    break;
            }
        }
    }

}
