package com.fedor.pavel.econtact.data;


public interface OnLoadNextListener {

    void loadNextPart(int offset);

    boolean canLoadMore();

}
