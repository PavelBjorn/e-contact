package com.fedor.pavel.econtact.adapters;


import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fedor.pavel.econtact.activities.ClaimFullInfoActivity;
import com.fedor.pavel.econtact.R;
import com.fedor.pavel.econtact.date.DateParseManager;
import com.fedor.pavel.econtact.fragments.ClaimsListFragment;
import com.fedor.pavel.econtact.models.AddressModel;
import com.fedor.pavel.econtact.models.ClaimModel;
import com.google.gson.Gson;


import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ClaimsListRVAdapter extends DataRvAdapter<ClaimModel, RecyclerView.ViewHolder> {

    private int mIndicator;

    public ClaimsListRVAdapter(ClaimsListFragment fragment) {
        super(fragment.getContext());
        mIndicator = fragment.getRequestType().getIndicatorResId();
    }

    public void clear() {
        int size = mItems.size();
        mItems.clear();
        notifyItemRangeRemoved(0, size);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = new ClaimsViewHolder(mInflater.inflate(R.layout.item_claims_list, parent, false));

        if (viewType==ViewTyp.VIEW_TYPE_FOOTER) {
            holder = new ProgressFooterViewHolder(mInflater.inflate(R.layout.item_loading_progress_rv_footer, parent, false));
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ClaimsViewHolder) {
            bindItemViewHolder((ClaimsViewHolder) holder, position);
        }
    }

    private void bindItemViewHolder(ClaimsViewHolder holder, int position) {

        ClaimModel claim = mItems.get(position);

        holder.mTvNumOfLikes.setText("" + claim.getNumOfLikes());
        holder.mTvCategory.setText(claim.getCategory().getTitle());
        holder.mTvDaysPassed.setText("-" + claim.getDaysPassed());

        AddressModel address = claim.getAddress();

        if (address != null) {
            holder.mTvAddress.setText(address.getFullAddress());
        }

        holder.mTvCreatedData.setText(DateParseManager.parseDate(claim.getStartedDate()
                , DateParseManager.MONTH_DAY_YEAR_DATE_PATTERN));

        holder.mImvIcon.setImageResource(R.drawable.ic_doc);
    }

    class ClaimsViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.item_claims_tv_numOfLikes)
        TextView mTvNumOfLikes;

        @Bind(R.id.item_claims_tv_category)
        TextView mTvCategory;

        @Bind(R.id.item_claims_tv_address)
        TextView mTvAddress;

        @Bind(R.id.item_claims_tv_createdDate)
        TextView mTvCreatedData;

        @Bind(R.id.item_claims_tv_daysPassed)
        TextView mTvDaysPassed;

        @Bind(R.id.item_claims_imv_icon)
        ImageView mImvIcon;


        public ClaimsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick(R.id.item_claims_content_container)
        public void startFullInfoActivity() {
            Intent intent = new Intent(itemView.getContext(), ClaimFullInfoActivity.class);
            intent.putExtra(ClaimsListFragment.STATE_INDICATOR_RES_ID_BUNDLE_KEY, mIndicator);
            intent.putExtra(ClaimsListFragment.BUNDLE_CLAIM_KEY, new Gson().toJson(mItems.get(getAdapterPosition())));
            itemView.getContext().startActivity(intent);
        }
    }

}
