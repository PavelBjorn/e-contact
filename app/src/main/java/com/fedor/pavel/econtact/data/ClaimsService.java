package com.fedor.pavel.econtact.data;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClaimsService {

    public static final String LOG_TAG = "ClaimsService";

    private ClaimsAPI mClaimsApi;

    public ClaimsService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(ClaimsAPI.QueryParams.BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        mClaimsApi = restAdapter.create(ClaimsAPI.class);
    }

    public ClaimsAPI getClaimsApi() {
        return mClaimsApi;
    }

}
