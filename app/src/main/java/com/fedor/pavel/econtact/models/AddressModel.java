package com.fedor.pavel.econtact.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.text.WordUtils;

public class AddressModel {

    @Expose
    @SerializedName(ApiFields.STREET_FIELD)
    private StreetModel mStreet;

    @Expose
    @SerializedName(ApiFields.HOUSE_FIELD)
    private HouseModel mHouse;


    public String getFullAddress() {
        return mStreet.getName() + ", " + mHouse.getNumber();
    }

    class StreetModel {

        @Expose
        @SerializedName(ApiFields.NAME_FIELD)
        private String mName;

        @Expose
        @SerializedName(ApiFields.TYPE_STREET_FIELD)
        private StreetTypeModel mStreetType;

        public StreetModel(String name) {
            this.mName = name;
        }

        public String getName() {
            return mStreetType.getShortName() + " " + mName;
        }

        public void setName(String mName) {
            this.mName = mName;
        }

        class StreetTypeModel {
            @SerializedName(ApiFields.SHORT_NAME_STREET_TYPE_FIELD)
            private String mShortName;

            public StreetTypeModel(String mShortName) {
                this.mShortName = mShortName;
            }

            public String getShortName() {
                return WordUtils.capitalize(mShortName);
            }

            public void setmShortName(String mShortName) {
                this.mShortName = mShortName;
            }
        }
    }

    class HouseModel {

        @Expose
        @SerializedName(ApiFields.NAME_FIELD)
        private String mNumber;

        public HouseModel(String number) {
            this.mNumber = number;
        }

        public String getNumber() {
            return mNumber;
        }

        public void setNumber(String mNumber) {
            this.mNumber = mNumber;
        }
    }

    public static class ApiFields {
        public static final String NAME_FIELD = "name";
        public static final String STREET_FIELD = "street";
        public static final String TYPE_STREET_FIELD = "street_type";
        public static final String SHORT_NAME_STREET_TYPE_FIELD = "short_name";
        public static final String HOUSE_FIELD = "house";
    }
}
