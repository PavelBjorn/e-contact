package com.fedor.pavel.econtact.models;


import android.support.annotation.DrawableRes;



public class RequestModel {

    private String mTitle;
    private int mIndicatorResId;
    private int[] mStateType;


    public static final int[] CLAIMS_STATE_IN_PROGRESS_VALUES = {0, 9, 5, 7, 8};
    public static final int[] CLAIMS_STATE_DONE_VALUES = {10, 6};
    public static final int[] CLAIMS_STATE_PENDING_VALUES = {1, 3, 4};

    public static final String REQUEST_TYPE_KEY = "RequestModel";

    public RequestModel(String title, int[] stateType, @DrawableRes int indicatorResId) {
        mTitle = title;
        mStateType = stateType;
        mIndicatorResId = indicatorResId;
    }


    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setStateType(int[] mStateType) {
        this.mStateType = mStateType;
    }

    public int getIndicatorResId(){
        return mIndicatorResId;
    }

    public int[] getRequestType() {
        return mStateType;
    }


}
