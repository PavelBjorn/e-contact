package com.fedor.pavel.econtact.date;




import org.apache.commons.lang3.text.WordUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class DateParseManager {

    public static final String MONTH_DAY_YEAR_DATE_PATTERN = "MMM dd, yyyy";
    public static final String DAY_MONTH_YEAR_DATE_PATTERN = "dd.MM.yyyy";

    public static String parseDate(long time, String pattern) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        return WordUtils.capitalize(dateFormat.format(calendar.getTime()));
    }

}
