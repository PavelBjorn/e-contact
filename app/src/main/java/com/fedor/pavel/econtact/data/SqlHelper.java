package com.fedor.pavel.econtact.data;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlHelper extends SQLiteOpenHelper {

    public static final String DATA_BASE_NAME = "e-contact.db";
    public static final int DATA_BASE_VERSION = 1;

    /*Table*/
    /*Claims table*/
    public static final String TABLE_NAME_CLAIMS = "claims";
    public static final String CLAIMS_ID_COLUMN = "_id";
    public static final String CLAIMS_SERVER_ID_COLUMN = "server_id";
    public static final String CLAIMS_REGISTERED_ID_COLUMN = "registered_id";
    public static final String CLAIMS_CATEGORY_COLUMN = "category";
    public static final String CLAIMS_ADDRESS_COLUMN = "address";
    public static final String CLAIMS_CREATED_DATE_COLUMN = "created_date";
    public static final String CLAIMS_STARTED_DATE_COLUMN = "registered_date";
    public static final String CLAIMS_DEADLINE_DATE_COLUMN = "deadline_date";
    public static final String ClAIMS_DESCRIPTION_COLUMN = "description";
    public static final String CLAIMS_NUM_OFF_LIKES_COLUMN = "num_of_likes";
    public static final String CLAIMS_PERFORMERS_COLUMN = "performers";
    public static final String CLAIMS_PHOTOS_COLUMN = "photos";
    public static final String CLAIMS_STATE_COLUMN = "state";

    /*User Table*/
    public static final String TABLE_NAME_USER = "user";
    public static final String USER_ID_COLUMN = "_id";
    public static final String USER_SERVER_ID_COLUMN = "server_id";
    public static final String USER_FIRST_NAME_COLUMN = "first_name";
    public static final String USER_LAST_NAME_COLUMN = "last_name";
    public static final String USER_PICTURE_COLUMN = "picture";
    public static final String USER_FACEBOOK_ACCESS_TOKEN = "access_token";

    /*Queries*/
    public static final String CREATE_TABLE_CLAIMS = "CREATE TABLE " + TABLE_NAME_CLAIMS + " ("
            + CLAIMS_ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CLAIMS_SERVER_ID_COLUMN + " INTEGER NOT NULL, "
            + CLAIMS_STATE_COLUMN + " TEXT, "
            + CLAIMS_REGISTERED_ID_COLUMN + " TEXT, "
            + CLAIMS_CATEGORY_COLUMN + " TEXT, "
            + CLAIMS_ADDRESS_COLUMN + " TEXT, "
            + CLAIMS_CREATED_DATE_COLUMN + " INTEGER, "
            + CLAIMS_STARTED_DATE_COLUMN + " INTEGER, "
            + CLAIMS_DEADLINE_DATE_COLUMN + " INTEGER, "
            + ClAIMS_DESCRIPTION_COLUMN + " TEXT, "
            + CLAIMS_NUM_OFF_LIKES_COLUMN + " INTEGER, "
            + CLAIMS_PERFORMERS_COLUMN + " TEXT, "
            + CLAIMS_PHOTOS_COLUMN + " TEXT"
            + ");";

    public static final String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_NAME_USER + "("
            + USER_ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + USER_SERVER_ID_COLUMN + " TEXT, "
            + USER_FIRST_NAME_COLUMN + " TEXT, "
            + USER_LAST_NAME_COLUMN + " TEXT, "
            + USER_PICTURE_COLUMN + " TEXT, "
            + USER_FACEBOOK_ACCESS_TOKEN + " TEXT"
            + ");";


    public static final String DROP_TABLE = "DROP TABLE ";

    public SqlHelper(Context context) {
        super(context, DATA_BASE_NAME, null, DATA_BASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CLAIMS);
        db.execSQL(CREATE_TABLE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE + TABLE_NAME_CLAIMS);
        db.execSQL(DROP_TABLE + TABLE_NAME_USER);
        onCreate(db);
    }
}
