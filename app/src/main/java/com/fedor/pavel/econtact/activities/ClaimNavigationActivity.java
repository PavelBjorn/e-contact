package com.fedor.pavel.econtact.activities;


import android.content.Intent;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.fedor.pavel.econtact.R;
import com.fedor.pavel.econtact.data.SqlManager;
import com.fedor.pavel.econtact.fragments.AllClaimsNavFragment;
import com.fedor.pavel.econtact.fragments.ClaimsListFragment;
import com.fedor.pavel.econtact.views.DrawerMenu;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ClaimNavigationActivity extends ProgressActivity
        implements DrawerMenu.OnDrawerMenuItemSelectedListener, ClaimsListFragment.ClaimsListFragFragmentCallback {

    @Bind(R.id.activity_claim_navigation_content_fab)
    FloatingActionButton mFabAddClaim;

    @Bind(R.id.activity_claim_navigation_fragmentContainer)
    ViewGroup mFragmentContainer;

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawer;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.drawer_rv_menu)
    DrawerMenu mDrawerMenu;

    private Snackbar mSnackBarErrorMessage;
    private CallbackManager mRegFaceBookCallBakManager;


    public static final int ALL_CLAIMS_MENU_ITEM_POSITION = 1;
    public static final int CLAIMS_ON_MAP_MENU_ITEM_POSITION = 2;
    public static final int LOGIN_MENU_ITEM_POSITION = 3;
    public static final String FACE_BOOK_LOGIN_PERMISSION = "public_profile";

    private  List<Integer> mStartedThreads = new ArrayList<>();
    public static final String LOG_TAG = "ClaimNavActivity";
    public static final String ERROR_MESSAGE = "Connection failed! Check the settings and try again";


    /**
     * Constant array contains position of background gradient in percentage.
     * Num of items in array equality to number of colors in gradient scheme
     */
    public static final float[] BACKGROUND_GRADIENT_POSITIONS = {0.10f, 0.15f, 0.20f, 0.85f, 0.90f, 1};


    private void prepareViews() {
        prepareToolbar();
        prepareFragmentContainer();
        prepareFab();
        prepareDrawerMenu();
        prepareSnack();
    }

    private void prepareToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            mDrawer.addDrawerListener(toggle);
            toggle.syncState();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        } else {
            Log.d(LOG_TAG, "Support action bar is null");
        }
    }

    private void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    private void prepareFragmentContainer() {
        ShapeDrawable.ShaderFactory shaderFactory = new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int width, int height) {
                return new LinearGradient(0, 0, 0, mFragmentContainer.getHeight()
                        , getResources().getIntArray(R.array.colors)
                        , BACKGROUND_GRADIENT_POSITIONS
                        , Shader.TileMode.REPEAT);
            }
        };

        PaintDrawable p = new PaintDrawable();
        p.setShape(new RectShape());
        p.setShaderFactory(shaderFactory);

        mFragmentContainer.setBackground(p);
    }

    private void prepareFab() {
        mFabAddClaim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void prepareDrawerMenu() {
        mDrawerMenu.setOnItemSelectedListener(this);
    }

    private void prepareSnack() {
        mSnackBarErrorMessage = Snackbar.make(mFabAddClaim
                , ERROR_MESSAGE
                , Snackbar.LENGTH_INDEFINITE);
    }

    public void replaceFragment(Fragment fragment, int containerId, int menuPosition) {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(containerId, fragment);
        transaction.commit();
        mDrawerMenu.setSelectedItem(menuPosition);
        setToolbarTitle(((DrawerMenu.MenuAdapter) mDrawerMenu.getAdapter()).getItem(menuPosition).getTitle());
    }

    private void loginWithFB() {
        if (AccessToken.getCurrentAccessToken() == null) {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(FACE_BOOK_LOGIN_PERMISSION));
        } else {
            startActivity(new Intent(this, ProfileActivity.class));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setProgressEnable();
        setContentView(R.layout.activity_claim_navigation);
        SqlManager.getInstance().open();
        ButterKnife.bind(this);
        prepareViews();
        replaceFragment(AllClaimsNavFragment.newInstance(), R.id.activity_claim_navigation_fragmentContainer
                , ALL_CLAIMS_MENU_ITEM_POSITION);
        mRegFaceBookCallBakManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mRegFaceBookCallBakManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                startActivity(new Intent(ClaimNavigationActivity.this, ProfileActivity.class));
            }

            @Override
            public void onCancel() {
                Toast.makeText(ClaimNavigationActivity.this, "Canceled ", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(ClaimNavigationActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        SqlManager.getInstance().close();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.claim_navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_filters:
                //// TODO: 16.04.2016 show some filters
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMenuItemSelected(DrawerMenu parent, int position) {
        mDrawer.closeDrawer(GravityCompat.START);

        switch (position) {
            case ALL_CLAIMS_MENU_ITEM_POSITION:
                replaceFragment(AllClaimsNavFragment.newInstance(), R.id.activity_claim_navigation_fragmentContainer, position);
                break;
            case CLAIMS_ON_MAP_MENU_ITEM_POSITION:

                break;

            case LOGIN_MENU_ITEM_POSITION:
                loginWithFB();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mRegFaceBookCallBakManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showErrorMessage(boolean show) {
        if (show) {
            if (!mSnackBarErrorMessage.isShown()) {
                mSnackBarErrorMessage.show();
            }
        } else {
            mSnackBarErrorMessage.dismiss();
        }
    }

    @Override
    public void startProgress(int threadId) {
        mStartedThreads.add(threadId);
        showProgress();
    }

    @Override
    public void endProgress(int threadId) {
        mStartedThreads.remove(Integer.valueOf(threadId));
        if (mStartedThreads.size()<=0){
            dismissProgress();
        }
    }

}
