package com.fedor.pavel.econtact.models;


import com.google.gson.annotations.SerializedName;

public class PerformersModel {

    @SerializedName(ApiFields.SERVER_ID_FIELD)
    private long mServerId;

    @SerializedName(ApiFields.ORGANIZATION_FIELD)
    private String mOrganization;

    @SerializedName(ApiFields.PERSON_FIELD)
    private String mPerson;

    public PerformersModel(long mServerId, String mOrganization, String mPerson) {
        this.mServerId = mServerId;
        this.mOrganization = mOrganization;
        this.mPerson = mPerson;
    }

    public long getmServerId() {
        return mServerId;
    }

    public void setmServerId(long mServerId) {
        this.mServerId = mServerId;
    }

    public String getOrganization() {
        return mOrganization;
    }

    public void setmOrganization(String mOrganization) {
        this.mOrganization = mOrganization;
    }

    public String getmPerson() {
        return mPerson;
    }

    public void setmPerson(String mPerson) {
        this.mPerson = mPerson;
    }

    public static class ApiFields {
        public static final String SERVER_ID_FIELD = "id";
        public static final String ORGANIZATION_FIELD = "organization";
        public static final String PERSON_FIELD = "person";
    }
}
