package com.fedor.pavel.econtact.views;

import android.animation.Animator;
import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;


public class HideWithScrollFab extends FloatingActionButton {

    private boolean mIsAnimationInProgress = false;

    public HideWithScrollFab(Context context) {
        super(context);
    }

    public HideWithScrollFab(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HideWithScrollFab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void show() {
        if (!mIsAnimationInProgress && getVisibility() == GONE) {
            animate()
                    .translationY(0)
                    .setInterpolator(new DecelerateInterpolator(2))
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            setVisibility(View.VISIBLE);
                            mIsAnimationInProgress = true;
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mIsAnimationInProgress = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            mIsAnimationInProgress = false;
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
                    .start();
        }
    }

    @Override
    public void hide() {
        if (!mIsAnimationInProgress && getVisibility() == VISIBLE) {
            animate()
                    .translationY(getHeight() + ((CoordinatorLayout.LayoutParams) getLayoutParams()).bottomMargin)
                    .setInterpolator(new AccelerateInterpolator(2))
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            mIsAnimationInProgress = true;
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            setVisibility(View.GONE);
                            mIsAnimationInProgress = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            mIsAnimationInProgress = false;
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
                    .start();
        }
    }
}
