package com.fedor.pavel.econtact.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fedor.pavel.econtact.R;
import com.fedor.pavel.econtact.adapters.ClaimsListRVAdapter;
import com.fedor.pavel.econtact.presenters.ClaimsPresenter;
import com.fedor.pavel.econtact.data.ClaimsService;
import com.fedor.pavel.econtact.data.OnDataLoadedListener;
import com.fedor.pavel.econtact.data.OnLoadNextListener;
import com.fedor.pavel.econtact.models.ClaimModel;
import com.fedor.pavel.econtact.models.RequestModel;
import com.fedor.pavel.econtact.views.RVDivider;
import com.google.gson.Gson;


import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ClaimsListFragment extends Fragment implements OnLoadNextListener, OnDataLoadedListener<List<ClaimModel>> {

    @Bind(R.id.fragment_claims_list_rv)
    RecyclerView mRvClaimsList;

    @Bind(R.id.fragment_claims_list_srl)
    SwipeRefreshLayout mSrlRefreshClaims;

    private ClaimsListFragFragmentCallback mCallback;
    private ClaimsListRVAdapter mRvAdapter;
    private ClaimsService mClaimsService;
    private ClaimsPresenter mClaimsPresenter;
    private RequestModel mRequestType;
    public int mFragmentPosition;
    private boolean mCanLoadMore = true;
    public static final String BUNDLE_CLAIM_KEY = "claim";
    public static final String STATE_INDICATOR_RES_ID_BUNDLE_KEY = "indicator";
    public static final String FRAGMENT_POSITION_BUNDLE_KEY = "position";
    public static final int API_CLAIMS_LIMIT = 20;
    public static final String LOG_TAG = "ClaimsListFragment";



    public static ClaimsListFragment newInstance(RequestModel stateModel,int position) {
        ClaimsListFragment instance = new ClaimsListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(RequestModel.REQUEST_TYPE_KEY,
                new Gson().toJson(stateModel));
        bundle.putInt(FRAGMENT_POSITION_BUNDLE_KEY,position);
        instance.setArguments(bundle);
        return instance;
    }

    private void prepareViews(View view) {
        ButterKnife.bind(this, view);
        prepareRecyclerView();
        prepareRefreshLayout();
    }

    private void prepareRecyclerView() {
        mRvAdapter = new ClaimsListRVAdapter(this);
        mRvClaimsList.setAdapter(mRvAdapter);
        mRvClaimsList.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvClaimsList.addItemDecoration(new RVDivider(getContext(), R.dimen.itemClaimsListOffset, LinearLayoutManager.VERTICAL));
        mRvAdapter.loadWithPagination(mRvClaimsList, this);
    }

    private void prepareRefreshLayout() {
        mSrlRefreshClaims.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mClaimsPresenter.loadClaims(mRequestType.getRequestType(), true);
            }
        });
    }

    public RequestModel getRequestType() {
        return mRequestType;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_claims_list, container, false);

        if (getActivity() instanceof ClaimsListFragFragmentCallback) {
            mCallback = (ClaimsListFragFragmentCallback) getActivity();
        }

        if (getArguments() != null) {
            mRequestType = new Gson().fromJson(getArguments().getString(RequestModel.REQUEST_TYPE_KEY), RequestModel.class);
            mFragmentPosition = getArguments().getInt(FRAGMENT_POSITION_BUNDLE_KEY);
        }
        prepareViews(view);
        mClaimsService = new ClaimsService();
        mClaimsPresenter = new ClaimsPresenter(this, mClaimsService);
        mClaimsPresenter.loadClaims(mRequestType.getRequestType(), false);

        return view;
    }

    @Override
    public void loadNextPart(int offset) {
        mClaimsPresenter.loadClaimsFromApi(mRequestType.getRequestType(), API_CLAIMS_LIMIT, offset)
                .subscribe(mClaimsPresenter.subscribeOnClaimsList());
    }

    @Override
    public boolean canLoadMore() {
        return mCanLoadMore;
    }

    @Override
    public void dataLoadingCompleted(List<ClaimModel> result) {
        mCallback.endProgress(mFragmentPosition);
        mCanLoadMore = result.size() == API_CLAIMS_LIMIT;
        mCallback.showErrorMessage(false);

        if (mRvAdapter.isIsLoadingData()) {
            mRvAdapter.setLoadingData(false);
        }
        if (mSrlRefreshClaims.isRefreshing()) {
            mRvAdapter.clear();
            mSrlRefreshClaims.setRefreshing(false);
        }
        mRvAdapter.addAll(result);
    }

    @Override
    public void loadingFailed(Throwable e) {
        mCallback.endProgress(mFragmentPosition);
        mRvAdapter.setLoadingData(false);
        if (mSrlRefreshClaims.isRefreshing()) {
            mSrlRefreshClaims.setRefreshing(false);
        }
        Log.d(ClaimsListFragment.LOG_TAG, e.toString());

        mCallback.showErrorMessage(true);
    }

    @Override
    public void loadingStarted() {
        if(!mSrlRefreshClaims.isRefreshing()){
        mCallback.startProgress(mFragmentPosition);
        }
    }

    public interface ClaimsListFragFragmentCallback {
        void showErrorMessage(boolean show);
        void startProgress(int threadId);
        void endProgress(int threadId);
    }

}
