package com.fedor.pavel.econtact.models;


import android.content.ContentValues;
import android.database.Cursor;

import com.fedor.pavel.econtact.data.SqlHelper;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName(FBFields.USER_ID_FB_FIELD)
    private String mUserId;

    @SerializedName(FBFields.FIRST_NAME_FB_FIELD)
    private String mFirstName;

    @SerializedName(FBFields.LAST_NAME_FB_FIELD)
    private String mLastName;

    @SerializedName(FBFields.PICTURE_FB_FIELD)
    private PictureModel mUserPicture;


    public UserModel(String userId, String firstName, String lastName, PictureModel userPicture) {
        mUserId = userId;
        mFirstName = firstName;
        mLastName = lastName;
        mUserPicture = userPicture;
    }

    public UserModel(Cursor cursor) {
        mUserId = cursor.getString(cursor.getColumnIndex(SqlHelper.USER_SERVER_ID_COLUMN));
        mFirstName = cursor.getString(cursor.getColumnIndex(SqlHelper.USER_FIRST_NAME_COLUMN));
        mLastName = cursor.getString(cursor.getColumnIndex(SqlHelper.USER_LAST_NAME_COLUMN));
        mUserPicture = new Gson().fromJson(cursor.getString(cursor.getColumnIndex(SqlHelper.USER_PICTURE_COLUMN)), PictureModel.class);
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getUserPictureUrl() {
        return mUserPicture.getPictureData().getUrl();
    }

    public void setmUserPicture(PictureModel mUserPicture) {
        this.mUserPicture = mUserPicture;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SqlHelper.USER_SERVER_ID_COLUMN, mUserId);
        contentValues.put(SqlHelper.USER_FIRST_NAME_COLUMN, mFirstName);
        contentValues.put(SqlHelper.USER_LAST_NAME_COLUMN, mLastName);
        contentValues.put(SqlHelper.USER_PICTURE_COLUMN, new Gson().toJson(mUserPicture));
        return contentValues;
    }

    class PictureModel {

        @SerializedName(FBFields.PICTURE_DATA_FB_FIELD)
        private DataModel mPictureData;

        public DataModel getPictureData() {
            return mPictureData;
        }

        class DataModel {

            @SerializedName(FBFields.PICTURE_DATA_URL_FB_FIELD)
            String mUrl;

            public String getUrl() {
                return mUrl;
            }
        }
    }

    public static class FBFields {
        public static final String USER_ID_FB_FIELD = "id";
        public static final String FIRST_NAME_FB_FIELD = "first_name";
        public static final String LAST_NAME_FB_FIELD = "last_name";
        public static final String PICTURE_FB_FIELD = "picture";
        public static final String PICTURE_TYPE_LARGE_FB_FIELD = ".type(large)";
        public static final String PICTURE_DATA_FB_FIELD = "data";
        public static final String PICTURE_DATA_URL_FB_FIELD = "url";
    }
}
