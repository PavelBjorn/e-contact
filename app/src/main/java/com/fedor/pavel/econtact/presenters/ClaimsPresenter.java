package com.fedor.pavel.econtact.presenters;


import android.os.Bundle;

import com.fedor.pavel.econtact.data.ClaimsAPI;
import com.fedor.pavel.econtact.data.ClaimsService;
import com.fedor.pavel.econtact.data.OnDataLoadedListener;
import com.fedor.pavel.econtact.data.SqlHelper;
import com.fedor.pavel.econtact.data.SqlManager;
import com.fedor.pavel.econtact.fragments.ClaimsListFragment;
import com.fedor.pavel.econtact.models.ClaimModel;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


public class ClaimsPresenter {

    private OnDataLoadedListener<List<ClaimModel>> mListener;
    private ClaimsService mClaimsService;
    public static final String REQUEST_PARAMS_BUNDLE_KEY = "queryParams";
    public static final String IS_REFRESHING_BUNDLE_KEY = "isRefresh";

    public ClaimsPresenter(OnDataLoadedListener listener, ClaimsService claimsService) {
        mListener = listener;
        mClaimsService = claimsService;
    }

    public void loadClaims(int[] queryParams, boolean isRefresh) {

        mListener.loadingStarted();
        Bundle bundle = new Bundle();
        bundle.putIntArray(REQUEST_PARAMS_BUNDLE_KEY, queryParams);
        bundle.putBoolean(IS_REFRESHING_BUNDLE_KEY, isRefresh);

        Observable.just(bundle)
                .subscribeOn(Schedulers.newThread())
                .flatMap(new Func1<Bundle, Observable<List<ClaimModel>>>() {
                             @Override
                             public Observable<List<ClaimModel>> call(Bundle bundle) {
                                 int[] queryParams = bundle.getIntArray(REQUEST_PARAMS_BUNDLE_KEY);
                                 Observable<List<ClaimModel>> observable;
                                 if (bundle.getBoolean(IS_REFRESHING_BUNDLE_KEY)) {
                                     SqlManager.getInstance().clearTableClaims(queryParams);
                                 }
                                 if (SqlManager.getInstance().isTableHaveData(SqlHelper.TABLE_NAME_CLAIMS, queryParams)) {
                                     observable = loadClaimsFromApi(queryParams, ClaimsListFragment.API_CLAIMS_LIMIT, 0);
                                 } else {
                                     observable = loadClaimsFromDb(queryParams);
                                 }

                                 return observable;
                             }
                         }
                ).subscribe(subscribeOnClaimsList());
    }

    public Observable<List<ClaimModel>> loadClaimsFromApi(int[] requestType, int amount, int offset) {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put(ClaimsAPI.QueryParams.CLAIMS_STATE__PARAM,
                Arrays.toString(requestType).replace("[", "").replace("]", "").replace(" ", ""));
        queryParams.put(ClaimsAPI.QueryParams.CLAIMS_AMOUNT_PARAM, "" + amount);
        queryParams.put(ClaimsAPI.QueryParams.CLAIMS_OFFSET_PARAM, "" + offset);

        return mClaimsService.getClaimsApi().getClaims(queryParams)
                .subscribeOn(Schedulers.newThread())
                .map(SqlManager.getInstance().insertClaims())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<ClaimModel>> loadClaimsFromDb(int[] requestType) {
        return SqlManager.getInstance().loadClaims(requestType)
                .subscribeOn(Schedulers.newThread())
                .map(SqlManager.getInstance().makeClaimsSelectQuery())
                .map(SqlManager.getInstance().parseToClaimsModels())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observer<List<ClaimModel>> subscribeOnClaimsList() {
        return new Observer<List<ClaimModel>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                mListener.loadingFailed(e);
            }

            @Override
            public void onNext(List<ClaimModel> claimModels) {
                mListener.dataLoadingCompleted(claimModels);
            }
        };
    }

}
