package com.fedor.pavel.econtact.presenters;


import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.fedor.pavel.econtact.data.OnDataLoadedListener;
import com.fedor.pavel.econtact.data.SqlHelper;
import com.fedor.pavel.econtact.data.SqlManager;
import com.fedor.pavel.econtact.models.UserModel;
import com.google.gson.Gson;


import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class ProfilePresenter {

    private OnDataLoadedListener<UserModel> mListener;
    public static final String FACE_BOOK_GRAPH_PATH = "me";

    public ProfilePresenter(OnDataLoadedListener<UserModel> userLoadedListener) {
        mListener = userLoadedListener;
    }

    public void loadUser() {
        mListener.loadingStarted();
        Bundle params = new Bundle();
        params.putString(GraphRequest.FIELDS_PARAM, UserModel.FBFields.USER_ID_FB_FIELD
                + "," + UserModel.FBFields.FIRST_NAME_FB_FIELD
                + "," + UserModel.FBFields.LAST_NAME_FB_FIELD
                + "," + UserModel.FBFields.PICTURE_FB_FIELD + UserModel.FBFields.PICTURE_TYPE_LARGE_FB_FIELD);

        Observable.just(params)
                .subscribeOn(Schedulers.newThread())
                .flatMap(new Func1<Bundle, Observable<UserModel>>() {
                    @Override
                    public Observable<UserModel> call(Bundle params) {
                        Observable<UserModel> observable;
                        if (SqlManager.getInstance().isTableHaveData(SqlHelper.TABLE_NAME_USER, null)) {
                            observable = loadUserFromApi(params);
                        } else {
                            observable = loadUserFromDb();
                        }
                        return observable;
                    }
                }).subscribe(observeResult());

    }

    private Observable<UserModel> loadUserFromDb() {
        return Observable.just(SqlHelper.TABLE_NAME_USER)
                .subscribeOn(Schedulers.newThread())
                .map(SqlManager.getInstance().makeSelectQuery())
                .map(SqlManager.getInstance().parseToUserModel())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<UserModel> loadUserFromApi(Bundle params) {
        return Observable.just(params)
                .subscribeOn(Schedulers.newThread())
                .map(new Func1<Bundle, UserModel>() {
                    @Override
                    public UserModel call(Bundle params) {
                        GraphRequest currentUserRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), FACE_BOOK_GRAPH_PATH, params, HttpMethod.GET);
                        currentUserRequest.setParameters(params);
                        return new Gson().fromJson(currentUserRequest.executeAndWait().getJSONObject().toString(), UserModel.class);
                    }
                })
                .map(SqlManager.getInstance().insertUser())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observer<UserModel> observeResult() {
        return new Observer<UserModel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                mListener.loadingFailed(e);
            }

            @Override
            public void onNext(UserModel userModel) {
                mListener.dataLoadingCompleted(userModel);
            }
        };
    }


}
