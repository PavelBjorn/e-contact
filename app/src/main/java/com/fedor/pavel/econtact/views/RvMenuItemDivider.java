package com.fedor.pavel.econtact.views;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fedor.pavel.econtact.R;


public class RvMenuItemDivider extends RecyclerView.ItemDecoration {

    private int mItemOffsetRight = 0;
    private int mItemOffsetBottom = 0;
    private int mToSubItemOffsetBottom = 0;
    private int mItemOffsetTop = 0;
    private int mItemOffsetLeft = 0;
    private int mDividerTopMargin = 0;
    private Drawable mDivider;
    public static final int DIVIDER_OPACITY_PERCENTAGE = (int) Math.round(((float) (255 * 12) / 100));


    public RvMenuItemDivider(Context context) {
        this.mItemOffsetBottom = context.getResources().getDimensionPixelSize(R.dimen.navItemsDividerBottomOffset);
        this.mToSubItemOffsetBottom = context.getResources().getDimensionPixelSize(R.dimen.navItemsDividerSubMenuOffset);
        this.mDividerTopMargin = context.getResources().getDimensionPixelSize(R.dimen.activity_vertical_margin);
        this.mDivider = context.getResources().getDrawable(R.drawable.divider);
    }


    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
        drawVertical(c, parent);
    }

    private void drawVertical(Canvas c, RecyclerView parent) {
        View view = parent.getChildAt(findLastItemPosition(parent));
        int top = view.getBottom() + mDividerTopMargin;
        int bottom = top + mDivider.getIntrinsicHeight();
        int right = parent.getWidth();
        int left = 0;
        mDivider.setAlpha(DIVIDER_OPACITY_PERCENTAGE);
        mDivider.getIntrinsicHeight();
        mDivider.setBounds(left, top, right, bottom);
        mDivider.draw(c);

    }

    private int findLastItemPosition(RecyclerView parent) {
        if (parent.getAdapter() instanceof DrawerMenu.MenuAdapter) {
            return (parent.getAdapter().getItemCount()
                    - ((DrawerMenu.MenuAdapter) parent.getAdapter()).getOnlyFootersCount()) - 1;
        }
        return 0;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        int currentItemPosition = parent.getChildAdapterPosition(view);


        if (findLastItemPosition(parent)
                == currentItemPosition) {
            outRect.set(mItemOffsetLeft, mItemOffsetTop, mItemOffsetRight, mToSubItemOffsetBottom);
        }

        if ((parent.getAdapter().getItemCount() - 1) != currentItemPosition
                && currentItemPosition != 0
                && currentItemPosition != findLastItemPosition(parent)) {
            outRect.set(mItemOffsetLeft, mItemOffsetTop, mItemOffsetRight, mItemOffsetBottom);
        }
    }
}
