package com.fedor.pavel.econtact.models;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class CategoryModel implements Serializable {

    @Expose
    @SerializedName(ApiFields.NAME_FIELD)
    private String mTitle;

    public CategoryModel(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }


    public static class ApiFields {
        public static final String NAME_FIELD = "name";
    }
}
