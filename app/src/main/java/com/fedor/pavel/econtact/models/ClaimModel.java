package com.fedor.pavel.econtact.models;


import android.content.ContentValues;
import android.database.Cursor;

import com.fedor.pavel.econtact.data.ClaimsAPI;
import com.fedor.pavel.econtact.data.SqlHelper;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class ClaimModel {

    private long mId;

    @Expose
    @SerializedName(ApiFields.SERVER_ID_FIELD)
    private long mServerId;

    @Expose
    @SerializedName(ApiFields.CLAIM_ID_FIELD)
    private String mClaimRegisteredId;

    @Expose
    @SerializedName(ApiFields.CATEGORY_FIELD)
    private CategoryModel mCategory;

    @Expose
    @SerializedName(ApiFields.ADDRESS_FIELD)
    private AddressModel mAddress;

    @Expose
    @SerializedName(ApiFields.CREATED_DATE_FIELD)
    private long mCreatedDate;

    @Expose
    @SerializedName(ApiFields.START_DATE_FIELD)
    private long mStartedDate;

    @Expose
    @SerializedName(ApiFields.DEADLINE_DATE_FIELD)
    private long mExecuteUntil;

    @Expose
    @SerializedName(ApiFields.BODY_FIELD)
    private String mDescription;

    @Expose
    @SerializedName(ApiFields.STATE_FIELD)
    private StateModel mState;

    @Expose
    @SerializedName(ApiFields.LIKES_COUNTER_FIELD)
    private int mNumOfLikes;

    @Expose
    @SerializedName(ApiFields.PERFORMERS_FIELD)
    private List<PerformersModel> mPerformers = new ArrayList<>();

    @Expose
    @SerializedName(ApiFields.FILES_FIELD)
    private List<PhotosModel> mPhotos = new ArrayList<>();

    private boolean mIsInDataBase = false;

    /**
     * Constant consist from expression which serves for parsing milliseconds into days.
     * (ms/s)*(s/min)*(min/hour)*(hour/day)
     */
    public static final int DENOMINATOR_FOR_PARSING_PASSED_DAYS = 1000 * 60 * 60 * 24;

    public ClaimModel() {
    }

    public ClaimModel(Cursor cursor) {
        mId = cursor.getLong(cursor.getColumnIndex(SqlHelper.CLAIMS_ID_COLUMN));
        mServerId = cursor.getLong(cursor.getColumnIndex(SqlHelper.CLAIMS_SERVER_ID_COLUMN));
        mCreatedDate = cursor.getLong(cursor.getColumnIndex(SqlHelper.CLAIMS_CREATED_DATE_COLUMN));
        mStartedDate = cursor.getLong(cursor.getColumnIndex(SqlHelper.CLAIMS_STARTED_DATE_COLUMN));
        mExecuteUntil = cursor.getLong(cursor.getColumnIndex(SqlHelper.CLAIMS_STARTED_DATE_COLUMN));
        mDescription = cursor.getString(cursor.getColumnIndex(SqlHelper.ClAIMS_DESCRIPTION_COLUMN));
        mClaimRegisteredId = cursor.getString(cursor.getColumnIndex(SqlHelper.CLAIMS_REGISTERED_ID_COLUMN));
        mNumOfLikes = cursor.getInt(cursor.getColumnIndex(SqlHelper.CLAIMS_NUM_OFF_LIKES_COLUMN));

        String performersJson = cursor.getString(cursor.getColumnIndex(SqlHelper.CLAIMS_PERFORMERS_COLUMN));
        if (checkJsonResponse(performersJson)) {
            mPerformers = new Gson().fromJson(performersJson, new TypeToken<List<PerformersModel>>() {
            }.getType());
        }

        String photosJson = cursor.getString(cursor.getColumnIndex(SqlHelper.CLAIMS_PHOTOS_COLUMN));
        if (checkJsonResponse(photosJson)) {
            mPhotos = new Gson().fromJson(photosJson, new TypeToken<List<PhotosModel>>() {
            }.getType());
        }


        String categoryJson = cursor.getString(cursor.getColumnIndex(SqlHelper.CLAIMS_CATEGORY_COLUMN));
        if (checkJsonResponse(categoryJson)) {
            mCategory = new Gson().fromJson(categoryJson,CategoryModel.class);
        }

        String stateJson = cursor.getString(cursor.getColumnIndex(SqlHelper.CLAIMS_STATE_COLUMN));
        if(checkJsonResponse(stateJson)) {
            mState = new Gson().fromJson(stateJson,StateModel.class);
        }

        String addressJson = cursor.getString(cursor.getColumnIndex(SqlHelper.CLAIMS_ADDRESS_COLUMN));
        if (checkJsonResponse(addressJson)) {
            mAddress = new Gson().fromJson(addressJson,AddressModel.class);
        }
    }


    public void addAllPhotosURL(List<PhotosModel> photosURL) {
        this.mPhotos.addAll(photosURL);
    }

    public AddressModel getAddress() {
        return mAddress;
    }

    public List<PhotosModel> getAllURLs() {
        return new ArrayList<>(mPhotos);
    }

    public int getNumOfPhoto() {
        return mPhotos.size();
    }

    public long getCreatedDate() {
        return mCreatedDate;
    }

    public long getStartedDate() {
        return mStartedDate;
    }

    public long getExecuteUntil() {
        return mExecuteUntil;
    }

    public PerformersModel getInCharge() {
        if (mPerformers.size() > 0) {
            return mPerformers.get(0);
        }
        return null;
    }

    public String getClaimRegisteredId() {
        return mClaimRegisteredId;
    }

    public void setTitle(String title) {
        this.mClaimRegisteredId = title;
    }

    public CategoryModel getCategory() {
        return mCategory;
    }

    public String getDescription() {
        return mDescription;
    }

    public StateModel getWorkStatus() {
        return mState;
    }

    public int getNumOfLikes() {
        return mNumOfLikes;
    }

    public int getDaysPassed() {
        return (int) ((System.currentTimeMillis() - getCreatedDate()) / DENOMINATOR_FOR_PARSING_PASSED_DAYS);
    }

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    public boolean isInDataBase() {
        return mIsInDataBase;
    }

    public void setInDataBase(boolean mIsInDataBase) {
        this.mIsInDataBase = mIsInDataBase;
    }

    public long getServerId() {
        return mServerId;
    }

    public void setServerId(long mServerId) {
        this.mServerId = mServerId;
    }

    private boolean checkJsonResponse(String jsonResponse) {
        return jsonResponse != null && !jsonResponse.isEmpty();
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SqlHelper.CLAIMS_SERVER_ID_COLUMN, mServerId);
        contentValues.put(SqlHelper.CLAIMS_REGISTERED_ID_COLUMN, mClaimRegisteredId);

        if (mCategory != null) {
            contentValues.put(SqlHelper.CLAIMS_CATEGORY_COLUMN, new Gson().toJson(mCategory));
        }

        if (mAddress != null) {
            contentValues.put(SqlHelper.CLAIMS_ADDRESS_COLUMN, new Gson().toJson(mAddress));
        }

        contentValues.put(SqlHelper.CLAIMS_STARTED_DATE_COLUMN, mStartedDate);
        contentValues.put(SqlHelper.CLAIMS_CREATED_DATE_COLUMN, mCreatedDate);
        contentValues.put(SqlHelper.CLAIMS_DEADLINE_DATE_COLUMN, mExecuteUntil);
        contentValues.put(SqlHelper.ClAIMS_DESCRIPTION_COLUMN, mDescription);
        contentValues.put(SqlHelper.CLAIMS_STATE_COLUMN, new Gson().toJson(mState));
        contentValues.put(SqlHelper.CLAIMS_NUM_OFF_LIKES_COLUMN, mNumOfLikes);

        if (mPerformers.size() > 0) {
            contentValues.put(SqlHelper.CLAIMS_PERFORMERS_COLUMN, new Gson().toJson(mPerformers));
        }

        if (mPhotos.size() > 0) {
            contentValues.put(SqlHelper.CLAIMS_PHOTOS_COLUMN, new Gson().toJson(mPhotos));
        }

        return contentValues;
    }

    public class PhotosModel {

        @Expose
        @SerializedName(ApiFields.FILES_NAME_FIELD)
        private String mFileName;

        PhotosModel(String mFileName) {
            this.mFileName = mFileName;
        }

        public String getUrl() {
            return ClaimsAPI.QueryParams.PICTURE_URL + mFileName;
        }

        public void setName(String mName) {
            this.mFileName = mName;
        }
    }

    public static class ApiFields {
        public static final String SERVER_ID_FIELD = "id";
        public static final String CLAIM_ID_FIELD = "ticket_id";
        public static final String CATEGORY_FIELD = "category";
        public static final String STATE_FIELD = "state";
        public static final String CREATED_DATE_FIELD = "created_date";
        public static final String START_DATE_FIELD = "start_date";
        public static final String DEADLINE_DATE_FIELD = "deadline";
        public static final String ADDRESS_FIELD = "address";
        public static final String BODY_FIELD = "body";
        public static final String LIKES_COUNTER_FIELD = "likes_counter";
        public static final String PERFORMERS_FIELD = "performers";
        public static final String FILES_FIELD = "files";
        public static final String FILES_NAME_FIELD = "filename";
    }

}
