package com.fedor.pavel.econtact.activities;


import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fedor.pavel.econtact.R;
import com.fedor.pavel.econtact.data.OnDataLoadedListener;
import com.fedor.pavel.econtact.presenters.ProfilePresenter;
import com.fedor.pavel.econtact.models.UserModel;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProfileActivity extends ProgressActivity implements OnDataLoadedListener<UserModel> {

    @Bind(R.id.profile_activity_user_photo)
    ImageView mImvUserPhoto;
    @Bind(R.id.activity_profile_container)
    ViewGroup mContentContainer;

    private ProfilePresenter mPresenter;
    public static final String LOG_TAG = "ProfileActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setProgressEnable();
        prepareToolbar();
        prepareContainer();
        mPresenter = new ProfilePresenter(this);
        mPresenter.loadUser();
    }

    private void prepareToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    private void prepareContainer() {
        ShapeDrawable.ShaderFactory shaderFactory = new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int width, int height) {
                return new LinearGradient(0, 0, 0, mContentContainer.getHeight()
                        , getResources().getIntArray(R.array.colors)
                        , ClaimNavigationActivity.BACKGROUND_GRADIENT_POSITIONS
                        , Shader.TileMode.REPEAT);
            }
        };

        PaintDrawable p = new PaintDrawable();
        p.setShape(new RectShape());
        p.setShaderFactory(shaderFactory);

        mContentContainer.setBackground(p);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    @Override
    public void dataLoadingCompleted(UserModel result) {
        dismissProgress();
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle(result.getFirstName() + " " + result.getLastName());
        }

        ImageLoader.getInstance().displayImage(result.getUserPictureUrl(), mImvUserPhoto);
    }

    @Override
    public void loadingFailed(Throwable e) {
        dismissProgress();
        Log.d(LOG_TAG, e.toString());
    }

    @Override
    public void loadingStarted() {
        showProgress();
    }
}
